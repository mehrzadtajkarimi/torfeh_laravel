@component('backend.layouts.include.content', ['title' =>'پنل مدیریت'])
@slot('breadcrumb')
<li class="breadcrumb-item"><a href="#">خانه</a></li>
<li class="breadcrumb-item active">صفحه سریع</li>
@endslot

<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header no-border">
                <div class="d-flex justify-content-between">
                    <h3 class="card-title">کاربران آنلاین</h3>
                    <a href="javascript:void(0);">مشاهده گزارش</a>
                </div>
            </div>
            <div class="card-body">
                <div class="d-flex">
                    <p class="d-flex flex-column">
                        <span class="text-bold text-lg">۸۲۰</span>
                        <span>بازدید کننده در طول زمان</span>
                    </p>
                    <p class="mr-auto d-flex flex-column text-right">
                        <span class="text-success">
                            <i class="fa fa-arrow-up"></i> ۱۲.۵%
                        </span>
                        <span class="text-muted">از هفته گذشته</span>
                    </p>
                </div>
                <!-- /.d-flex -->

                <div class="position-relative mb-4">
                    <div style="position: absolute; inset: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;" class="chartjs-size-monitor">
                        <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                            <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
                        </div>
                        <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                            <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
                        </div>
                    </div>
                    <canvas id="visitors-chart" height="200" style="display: block; width: 489px; height: 200px;" width="489" class="chartjs-render-monitor"></canvas>
                </div>

                <div class="d-flex flex-row justify-content-end">
                    <span class="ml-2">
                        <i class="fa fa-square text-primary"></i> این هفته
                    </span>

                    <span>
                        <i class="fa fa-square text-gray"></i> هفته گذشته
                    </span>
                </div>
            </div>
        </div>
        <!-- /.card -->

        <div class="card">
            <div class="card-header no-border">
                <h3 class="card-title">محصولات</h3>
                <div class="card-tools">
                    <a href="#" class="btn btn-tool btn-sm">
                        <i class="fa fa-download"></i>
                    </a>
                    <a href="#" class="btn btn-tool btn-sm">
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
            </div>
            <div class="card-body p-0">
                <table class="table table-striped table-valign-middle">
                    <thead>
                        <tr>
                            <th>محصولات</th>
                            <th>قیمت</th>
                            <th>فروش</th>
                            <th>بیشتر</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                تلویزیون هوشمند
                            </td>
                            <td>۱۳ تومان</td>
                            <td>
                                <small class="text-success mr-1">
                                    <i class="fa fa-arrow-up"></i>
                                    ۱۲%
                                </small>
                                ۱۲,۰۰۰ فروش
                            </td>
                            <td>
                                <a href="#" class="text-muted">
                                    <i class="fa fa-search"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                محصول ایکس
                            </td>
                            <td>۲۹ تومان</td>
                            <td>
                                <small class="text-warning mr-1">
                                    <i class="fa fa-arrow-down"></i>
                                    ۰.۵%
                                </small>
                                ۱۲۳,۲۳۴ فروش
                            </td>
                            <td>
                                <a href="#" class="text-muted">
                                    <i class="fa fa-search"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                محصول پرفروش
                            </td>
                            <td>۱,۲۳۰ تومان</td>
                            <td>
                                <small class="text-danger mr-1">
                                    <i class="fa fa-arrow-down"></i>
                                    ۳%
                                </small>
                                ۱۹۸ فروش
                            </td>
                            <td>
                                <a href="#" class="text-muted">
                                    <i class="fa fa-search"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                محصول جدید
                                <span class="badge bg-danger">جدید</span>
                            </td>
                            <td>۱۹۹ تومان</td>
                            <td>
                                <small class="text-success mr-1">
                                    <i class="fa fa-arrow-up"></i>
                                    ۶۳%
                                </small>
                                ۸۷ فروش
                            </td>
                            <td>
                                <a href="#" class="text-muted">
                                    <i class="fa fa-search"></i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col-md-6 -->
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header no-border">
                <div class="d-flex justify-content-between">
                    <h3 class="card-title">فروش</h3>
                    <a href="javascript:void(0);">مشاهده گزارش</a>
                </div>
            </div>
            <div class="card-body">
                <div class="d-flex">
                    <p class="d-flex flex-column">
                        <span class="text-bold text-lg">تومان ۱۸,۲۳۰.۰۰</span>
                        <span>فروش در طول زمان</span>
                    </p>
                    <p class="mr-auto d-flex flex-column text-right">
                        <span class="text-success">
                            <i class="fa fa-arrow-up"></i> ۳۳.۱%
                        </span>
                        <span class="text-muted">از ماه گذشته</span>
                    </p>
                </div>
                <!-- /.d-flex -->

                <div class="position-relative mb-4">
                    <div style="position: absolute; inset: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;" class="chartjs-size-monitor">
                        <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                            <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
                        </div>
                        <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                            <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
                        </div>
                    </div>
                    <canvas id="sales-chart" height="200" style="display: block; width: 489px; height: 200px;" width="489" class="chartjs-render-monitor"></canvas>
                </div>

                <div class="d-flex flex-row justify-content-end">
                    <span class="ml-2">
                        <i class="fa fa-square text-primary"></i> امسال
                    </span>

                    <span>
                        <i class="fa fa-square text-gray"></i> سال گذشته
                    </span>
                </div>
            </div>
        </div>
        <!-- /.card -->

        <div class="card">
            <div class="card-header no-border">
                <h3 class="card-title">بررسی اجمالی فروشگاه آنلاین</h3>
                <div class="card-tools">
                    <a href="#" class="btn btn-sm btn-tool">
                        <i class="fa fa-download"></i>
                    </a>
                    <a href="#" class="btn btn-sm btn-tool">
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                    <p class="text-success text-xl">
                    <i class="fas fa-sync p-1" ></i>

                    </p>
                    <p class="d-flex flex-column text-right">
                        <span class="font-weight-bold">
                            <i class="fa fa-arrow-up text-success"></i> ۱۲%
                        </span>
                        <span class="text-muted">نرخ تبدیل</span>
                    </p>
                </div>
                <!-- /.d-flex -->
                <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                    <p class="text-warning text-xl">
                    <i class="fas fa-shopping-cart p-1"></i>
                    </p>
                    <p class="d-flex flex-column text-right">
                        <span class="font-weight-bold">
                            <i class="fa fa-arrow-up text-warning"></i> ۰.۸%
                        </span>
                        <span class="text-muted">نرخ فروش</span>
                    </p>
                </div>
                <!-- /.d-flex -->
                <div class="d-flex justify-content-between align-items-center mb-0">
                    <p class="text-danger text-xl">
                        <i class="fas fa-users p-1"></i>
                    </p>
                    <p class="d-flex flex-column text-right">
                        <span class="font-weight-bold">
                            <i class="fa fa-arrow-down text-danger"></i> ۱%
                        </span>
                        <span class="text-muted">نرخ ثبت نام</span>
                    </p>
                </div>
                <!-- /.d-flex -->
            </div>
        </div>
    </div>
    <!-- /.col-md-6 -->
</div>
@endcomponent
