<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<title>پنل مدیریت | شروع سریع</title>
<!-- <script src="https://kit.fontawesome.com/8e6fa7d1cb.js" crossorigin="anonymous"></script> -->

<!-- Font Awesome Icons -->
<link rel="stylesheet" href="{{ asset('/panel/plugins/font-awesome/css/all.css') }}">
<link rel="icon" sizes="114x114" type="image/x-icon" href="{{ asset('front/img/favicon.png') }}" />


<link rel="stylesheet" href="{{ asset('/panel/dist/css/ionicons.min.css') }}">
<link rel="stylesheet" href="{{ asset('/panel/plugins/select2/select2.css') }}">
<link rel="stylesheet" href="{{ asset('/panel/plugins/iCheck/all.css') }}">

<!-- Theme style -->
<link rel="stylesheet" href="{{ asset('/panel/dist/css/adminlte.min.css') }}">
<!-- Google Font: Source Sans Pro -->
<link href="{{ asset('/panel/dist/fonts/font.css') }}" rel="stylesheet">

<!-- bootstrap rtl -->
<link rel="stylesheet" href="{{ asset('/panel/dist/css/bootstrap-rtl.min.css') }}">
<!-- template rtl version -->
<link rel="stylesheet" href="{{ asset('/panel/dist/css/custom-style.css') }}">
<link rel="stylesheet" href="{{ asset('css/admin.css') }}">